var firstItem = {};
var secondItem = {};

function scan(objOne, objTwo, targetArray) {
    targetArray.forEach(function(entry) {

        var hasChildren = objectHasChildren(entry);

        if (entry.id == objOne.id) {
            firstItem = entry;
        } else if (entry.id == objTwo.id) {
            secondItem = entry;
        } else if (hasChildren) {
            scan(objOne, objTwo, entry.sub_items);
        }
    });
}

function objectHasChildren(object) {
    for (var property in object) {
        if (typeof object[property] == 'object') {
            if (object[property].length == 0) {
                return false;
            } else {
                return object[property];
            }
        }
    }
}